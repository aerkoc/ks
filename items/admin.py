from django.contrib import admin
from models import Item

# Register your models here.


class ItemAdmin(admin.ModelAdmin):
    search_fields = ['hostname']


admin.site.register(Item, ItemAdmin)