from django.shortcuts import render, render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from models import Item
from forms import ItemForm
from django.contrib.auth.models import User
# Create your views here.


@login_required(login_url='/login')
def home(request):
    user = User.objects.get(username=request.user)
    items = list(user.item_owner.all()) + list(user.item_acl.all())

    return render_to_response('index.html', {
        'items': items
    }, RequestContext(request))


@login_required(login_url='/login')
def new_item(request):
    form = ItemForm()

    if request.method == 'POST':
        form = ItemForm(request.POST)

        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect(reverse("home"))
        else:
            return HttpResponse("Lutfen formu kontrol ediniz !")
    return render_to_response('new.html', {
        'form': form
    }, RequestContext(request))


@login_required(login_url='/login')
def delete_item(request, pk):
    if Item.objects.get(id=pk).owner == request.user:
        Item.objects.get(id=pk).delete()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='/login')
def edit_item(request, pk):
    if Item.objects.get(id=pk).owner == request.user:
        item = Item.objects.get(id=pk)
        form = ItemForm(instance=item)

        if request.method == 'POST':
            form = ItemForm(request.POST, instance=item)
            if form.is_valid():
                form.save()
                return redirect(reverse("home"))
            else:
                return HttpResponse("Lutfen formu kontrol ediniz !")

        return render_to_response('edit.html', {
            'form': form,
            'item': item
        }, RequestContext(request))
    else:
        return redirect(reverse("home"))


@login_required(login_url='/login')
def permission_item_list(request, pk):
    item = Item.objects.get(id=pk)
    acls = item.acl.all
    users = User.objects.all().exclude(username=request.user)
    return render_to_response('permission_list.html', {
        'acls': acls,
        'item': item,
        'users': users
    }, RequestContext(request))


@login_required(login_url='/login')
def permission_item_add(request, pk):
    user_id = request.POST.get("user_id")
    Item.objects.get(id=pk).acl.add(User.objects.get(id=user_id))
    return redirect('permission_item_list', pk=pk)


@login_required(login_url='/login')
def permission_item_delete(request, pk, pk_alt):
    Item.objects.get(id=pk).acl.remove(User.objects.get(id=pk_alt))
    return redirect('permission_item_list', pk=pk)

