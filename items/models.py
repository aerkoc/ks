from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Item(models.Model):
    PROTOCOL_CHOICES = (
        (1, 'SSH'),
        (2, 'RDP'),
        (3, 'Telnet'),
    )
    owner = models.ForeignKey(User, related_name='item_owner')
    acl = models.ManyToManyField(User, related_name='item_acl')
    hostname = models.CharField(max_length=100)
    ip = models.GenericIPAddressField()
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    protocol = models.IntegerField(choices=PROTOCOL_CHOICES, default=1)
    description = models.TextField()

    def __unicode__(self):
        return '%s - %s' % (self.owner, self.hostname)


