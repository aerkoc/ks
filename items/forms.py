__author__ = 'erkoc'

from django import forms
from models import Item


class ItemForm(forms.ModelForm):

    class Meta:
        fields = ['hostname', 'ip', 'username', 'password', 'protocol', 'description']
        model = Item
