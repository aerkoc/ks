from django.conf.urls import patterns, include, url
from django.contrib import admin
import items.views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ksapp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^login$', 'profiles.views.login', name='login'),
    url(r'^logout', 'profiles.views.logout', name='logout'),
    url(r'^register', 'profiles.views.register', name='register'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', items.views.home, name='home'),
    url(r'new$', items.views.new_item, name='new_item'),
    url(r'delete/(?P<pk>[\d]+)$', items.views.delete_item, name='delete_item'),
    url(r'edit/(?P<pk>[\d]+)$', items.views.edit_item, name='edit_item'),
    url(r'permission/(?P<pk>[\d]+)$', items.views.permission_item_list, name='permission_item_list'),
    url(r'permission_add/(?P<pk>[\d]+)$', items.views.permission_item_add, name='permission_item_add'),
    url(r'permission_delete/(?P<pk>[\d]+)/(?P<pk_alt>[\d]+)$', items.views.permission_item_delete, name='permission_item_delete'),

)
